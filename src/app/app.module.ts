import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { HomeModule } from './home/home.module';
import { AppRountingModule } from './app.routing.module';
import { ErrorsModule } from './errors/errors.module';
import { ListaComponent } from './home/lista/lista.componet';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ErrorsModule,
    HomeModule,
    AppRountingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
