export interface Formulario{
    descricao: string;
    vencimento: Date;
    valor: number;
    tipo: string;
}