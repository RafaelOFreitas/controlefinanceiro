import { HttpClient } from "@angular/common/http";
import { Formulario } from "./formulario";
import { Injectable } from "@angular/core";

const API_URL = 'http://localhost:8080/controle_financeiro';

@Injectable({ providedIn: 'root' })
export class FormularioService {

   constructor(private http: HttpClient) { }

   //acesso a api, POST
   novoRegistro(newRegist: Formulario) {
      return this.http.post(API_URL + '/conta/cadastrar', newRegist);
   }

}
