import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { Formulario } from "./formulario";
import { FormularioService } from "./formulario.service";

@Component({
    templateUrl: './formulario.component.html'
})

export class FormularioComponent implements OnInit {

    contaForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private formularioService: FormularioService,
        private router: Router) { }

    //validacoes de formulario;
    ngOnInit(): void {
        this.contaForm = this.formBuilder.group({
            descricao: ['', Validators.required],
            vencimento: ['', Validators.required],
            valor: ['', Validators.required],
            tipo: ['', Validators.required]
        });
    }

    //metodo para novos registros
    cadastrar() {
        const newRegist = this.contaForm.getRawValue() as Formulario;
        this.formularioService
            .novoRegistro(newRegist)
            .subscribe(() => this.router.navigate(['']),
                err => console.log(err));
    }
}