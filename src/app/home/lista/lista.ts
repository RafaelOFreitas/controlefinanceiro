export interface Lista{
    id :Int16Array;
    descricao: string;
    vencimento: Date;
    valor: number;
    tipo: string;
}