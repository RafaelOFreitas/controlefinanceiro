import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Lista } from "./lista";

const API = 'http://localhost:8080/controle_financeiro/conta/';

@Injectable({ providedIn: 'root' })
export class ListaService {
    constructor(private http: HttpClient) {
    }

    listFromContas() {
        return this.http
            .get<Lista[]>(API + 'listar');
    }

    deleteFromContas(id) {
        this.http.delete(API + "excluir/", id)
            .subscribe();
    }
}
