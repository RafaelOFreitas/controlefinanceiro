import { Component, Input, OnInit } from "@angular/core";
import { ListaService } from "./lista.service";
import { Lista } from "./lista";

@Component({
    templateUrl: './lista.component.html'
})
export class ListaComponent implements OnInit {

    lista: Lista[];

    constructor(private listaService: ListaService) {
    }

    ngOnInit(): void {
        this.listaService.listFromContas()
            .subscribe(lista => this.lista = lista);
    }

    deleteConta(id): void {
        if (this.lista[0]) {
            this.listaService.deleteFromContas(id);
        }
    }
}
