import { NgModule } from '@angular/core'
import { HttpClientModule } from '@angular/common/http';
import { Http, HttpModule } from '@angular/http';
import { FormularioComponent } from './formulario/formulario.componet';
import { ListaComponent } from './lista/lista.componet';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        FormularioComponent,
        ListaComponent
    ],
    exports: [
        FormularioComponent,
        ListaComponent
    ],
    imports: [
        HttpModule,
        HttpClientModule,
        CommonModule,
        ReactiveFormsModule,
        RouterModule
    ]
})
export class HomeModule { }