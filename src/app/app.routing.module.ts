import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaComponent } from './home/lista/lista.componet';
import { FormularioComponent } from './home/formulario/formulario.componet';
import { NotFoundComponent } from './errors/not-found/not-found.component';


const routes: Routes = [
    { path: 'novo', component: FormularioComponent },
    { path: '', component: ListaComponent },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRountingModule {

}