<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Controle Financeiro</title>
</head>
<body>
	<h3>Adicionar Contas</h3>

	<form action="/ControleFinanceiro/lista" method="POST">
		<div>
			<label>Descricao</label>
			<textarea name="descricao" rows="5" cols="50"></textarea>
			<form:errors path="conta.descricao" />
			<br></br>
		</div>

		<div>
			<label>Valor</label> <input type="text" name="valor" />
			<form:errors path="conta.valor" />
			<br></br>
		</div>

		<div>
			<label>Vencimento</label> <input type="text" name="vencimento" />
			<form:errors path="conta.vencimento" />
			<br></br>
		</div>

		<div>
			<label>Tipo</label> <select name="tipo">
				<option value="RECEITA">RECEITA</option>
				<option value="DESPESA">DESPESA</option>
				<form:errors path="conta.tipo" />
			</select>
		</div>
		<br></br>
		<input type="submit" value="Acionar" />
		
	</form>
</body>
</html>