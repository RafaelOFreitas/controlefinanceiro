package br.com.controle.financeiro.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.controle.financeiro.models.Conta;

@Repository
public class ContaDAO implements InterfaceContaDao {

	@Autowired
	private ContaDAO contaDao;

	@Transactional
	public long save(Conta conta) {
		return contaDao.save(conta);
	}

	@Override
	public Conta get(long id) {
		return contaDao.get(id);
	}

	@Override
	public List<Conta> list() {
		return contaDao.list();
	}

	@Transactional
	@Override
	public void delete(long id) {
		contaDao.delete(id);
	}

	@Override
	public void update(long id, Conta conta) {
	}
}
