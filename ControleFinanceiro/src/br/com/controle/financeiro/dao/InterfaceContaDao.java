package br.com.controle.financeiro.dao;

import java.util.List;

import br.com.controle.financeiro.models.Conta;

public interface InterfaceContaDao {

	long save(Conta conta);

	Conta get(long id);

	List<Conta> list();

	void update(long id, Conta conta);

	void delete(long id);
}
