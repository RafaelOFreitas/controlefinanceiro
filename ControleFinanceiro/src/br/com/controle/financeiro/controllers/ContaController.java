package br.com.controle.financeiro.controllers;

import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.controle.financeiro.dao.ContaDAO;
import br.com.controle.financeiro.models.Conta;

@Path("/")
@RestController
public class ContaController {

	@Autowired
	private ContaDAO contaDao;

	@GetMapping("lista")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Conta> getcontas() {
		return contaDao.list();
	}

	@RequestMapping("/form")
	public String lista() {
		return "formulario";
	}

}
