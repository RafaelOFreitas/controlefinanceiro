package br.com.controle.financeiro.models;

public enum TipoDaConta {
	RECEITA, DESPESA;

	public String toStringR() {
		return RECEITA.toString();
	}

	public String toStringD() {
		return DESPESA.toString();
	}

}
