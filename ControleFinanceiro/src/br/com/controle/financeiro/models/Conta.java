package br.com.controle.financeiro.models;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Entity
public class Conta implements Validator {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String descricao;

	private TipoDaConta tipo;

	@DateTimeFormat
	private Calendar vencimento;
	
	private Double valor;

	public Conta() {
		super();
	}

	public Conta(Long id, String descricao, TipoDaConta tipo, Calendar vencimento, Double valor) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.tipo = tipo;
		this.vencimento = vencimento;
		this.valor = valor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoDaConta getTipo() {
		return tipo;
	}

	public void setTipo(TipoDaConta tipo) {
		this.tipo = tipo;
	}

	public Calendar getVencimento() {
		return vencimento;
	}

	public void setVencimento(Calendar vencimento) {
		this.vencimento = vencimento;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "Conta [id=" + id + ", descricao=" + descricao + ", tipo=" + tipo + ", vencimento=" + vencimento
				+ ", valor=" + valor + "]";
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return Conta.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "descricao", "field.required");
		ValidationUtils.rejectIfEmpty(errors, "tipo", "field.required");
		ValidationUtils.rejectIfEmpty(errors, "vencimento", "field.required");
		ValidationUtils.rejectIfEmpty(errors, "valor", "field.required");
	}

}
