package br.com.controle.financeiro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.controle.financeiro.dao.ContaDAO;
import br.com.controle.financeiro.models.Conta;


@Service
public class ContaService {

	@Autowired
	private ContaDAO contaDao;

	public List<Conta> getAllContas() {
		return contaDao.list();
	}

}